package com.borex.controllers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.borex.pojos.Event;
import com.borex.pojos.EventType;
import com.borex.services.EventService;

@RestController
@RequestMapping("/event")
public class EventController {
    private EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(method = RequestMethod.GET,
    		produces=MediaType.APPLICATION_JSON_VALUE)
	public String getEvents() {
		return "event";
    }
//    public List<Event> getEvents() {
//    	return Arrays.asList(Event.createEvent(EventType.PARTY, new Date(), "Rager"));
//    }
}
